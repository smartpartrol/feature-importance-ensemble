# Feature Importance Ensembler

Feature Importance Voting Ensembler to decide on the best features for multiple targets in classification or regression.

The purpose of this program is to allow for quick feature selection across a borad variety of metrics.


## Packages

Package versions are:

```
numpy==1.14.3
pandas==0.23.4
pyparsing==2.2.0
scikit-learn==0.19.1
```

## Usage

To clone this notebook:

`git clone https://gitlab.com/smartpartrol/feature-importance-ensemble.git`

The repo comes with a simple example to run on dummy data.
```
python run_feature_importance.py 
--in_file payload.csv 
--out_file test.csv 
--seed 123 
--regression False 
--njobs 4 
--targets y1 y2 
--features x1 x2 x3
```