"""
Main script to run feature importance ensemble.
"""

import argparse
import logging

from feature_main import *

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

np.set_printoptions(precision=5, suppress=True)

if __name__ == '__main__':

    # intializing arguments to pass
    parser = argparse.ArgumentParser()
    parser.add_argument('--in_file', type=str,
                        help='.csv file with named columns and features', required=True)
    parser.add_argument('--out_file', type=str,
                        help='.csv output file of feature importances', required=True)
    parser.add_argument('--seed', type=int, default=33,
                        help='random seed to set')
    parser.add_argument('--njobs', type=int, default=4, help='sklearn njobs')
    parser.add_argument('--regression', type=bool, default=True,
                        help='Use Regression? False = Classification')
    parser.add_argument('--targets', nargs='+',
                        help='list of targets', required=True)
    parser.add_argument('--features', nargs='+',
                        help='list of features', required=True)

    args = parser.parse_args()

    if args.regression == True:
        print("Running for Regression")
    else:
        print("Running for Classification")
    print("")
    print("These features will be used...")
    print(args.features, "\n")
    print("These targets will be used....")
    print(args.targets, "\n")

    # set macro level variables
    SEED = args.seed
    np.random.seed(SEED)
    NJOBS = args.njobs

    # readin base file
    df = pd.read_csv(args.in_file)

    # run ensemble feature importance
    outputs = run_feature_importance(
        df, args.features, args.targets, args.regression, SEED, NJOBS)
        
    #final results saved as .csv
    print("Outputting to file...", "\n")
    outputs.to_csv(args.out_file)
