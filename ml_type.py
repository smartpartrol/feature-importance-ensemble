"""
Intializes feature importance measures based on regression or classification task selected.
"""

# TO DO
# Add mixed task support for regression and classification targets

from xgboost import XGBRegressor

from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, RandomForestClassifier, ExtraTreesClassifier
from sklearn.svm import LinearSVR
from sklearn.linear_model import LinearRegression, Lasso, LogisticRegression
from sklearn.feature_selection import SelectFromModel, GenericUnivariateSelect, RFE, SelectKBest
from sklearn.feature_selection import f_regression, mutual_info_regression, mutual_info_classif, f_classif


def ml_type(SEED, NJOBS, regression_flag):

    if regression_flag == True:
        print("Outcome set as Regression")
        # Univariate scoring functions
        uni_imp_funcs = (f_regression, mutual_info_regression)
        # getting Tree-based importances
        rf = RandomForestRegressor(
            random_state=SEED, n_estimators=50, criterion='mae', n_jobs=NJOBS, min_samples_leaf=3)
        ext = ExtraTreesRegressor(
            n_estimators=50, criterion='mse', n_jobs=NJOBS, random_state=SEED)
        xgb = XGBRegressor(max_depth=3, learning_rate=0.01, n_estimators=50,
                           n_jobs=NJOBS, subsample=0.5, random_state=SEED)
        # getting RFE model
        lr = LinearRegression()
        # L1 regressor
        l1 = Lasso(random_state=SEED, alpha=0.1, tol=1e-6)

        return uni_imp_funcs, rf, ext, xgb, lr, l1

    else:
        print("Outcome set as Classification")
        # Univariate scoring functions
        uni_imp_funcs = (f_classif, mutual_info_classif)
        # getting Tree-based importances
        rf = RandomForestClassifier(
            random_state=SEED, n_estimators=50, criterion='entropy', n_jobs=NJOBS, min_samples_leaf=3)
        ext = ExtraTreesClassifier(
            n_estimators=100, criterion='gini', n_jobs=NJOBS, random_state=SEED)
        xgb = XGBClassifier(max_depth=3, learning_rate=0.01, n_estimators=100,
                            n_jobs=NJOBS, subsample=0.4, random_state=SEED)
        # getting RFE model
        lr = LogisticRegression()
        # L1 Classifier
        l1 = LinearSVC(C=1.0, penalty="l1", random_state=SEED,
                       max_iter=1000, dual=False)

        return uni_imp_funcs, rf, ext, xgb, lr, l1