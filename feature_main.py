"""
Main wrapper function to run the feature importance ensembler.
Takes:
df - data frame from .csv in file
features - list of features specified as an argument
targets - list of targets specified as an argument
regression_flag - True for Regression and False for Classification
SEED - the random seed set
NJOBS - the number of cores to run on for trees
Returns:
A dataframe of all measured importances with each feature and target pair as corresponding row.
"""

import pickle
import pandas as pd
import numpy as np

from xgboost import XGBRegressor

from sklearn.ensemble import RandomForestRegressor, ExtraTreesRegressor, RandomForestClassifier, ExtraTreesClassifier
from sklearn.svm import LinearSVR
from sklearn.linear_model import LinearRegression, Lasso, LogisticRegression
from sklearn.feature_selection import SelectFromModel, GenericUnivariateSelect, RFE, SelectKBest
from sklearn.feature_selection import f_regression, mutual_info_regression, mutual_info_classif, f_classif

from ml_type import *
from feature_importance_util import *


def run_feature_importance(df, features, targets, regression_flag, SEED, NJOBS):
    """Warapper Functin for the process"""
    uni_imp_funcs, rf, ext, xgb, lr, l1 = ml_type(SEED, NJOBS, regression_flag)
    # get F-Score and Mutual Info Criteria
    print("Getting F-Score and Mutual Information Criteria", "\n")
    f_importances, mu_importances = make_univ_imp(
        df, uni_imp_funcs, features, targets)
    f_imp_df = stack_importances(f_importances, targets)
    mu_imp_df = stack_importances(mu_importances, targets)

    print("Making RF Importances...", "\n")
    rf_imp = make_trees(df, rf, features, targets)
    print("Making Extra Trees Importances...", "\n")
    ext_imp = make_trees(df, ext, features, targets)
    print("Making XGBs Importances...", "\n")
    xgb_imp = make_trees(df, xgb, features, targets)

    print("Running Recursive Featurea Elimination", "\n")
    rfe_imp = run_recursive_elmination(df, lr, targets, features)

    print("Running L1 Selection", "\n")
    l1_imp = get_l1_importance(df, l1, targets, features)

    print("Generating Feature Importance Report...", "\n")

    base = pd.melt(f_imp_df, id_vars='index')

    for m in [mu_imp_df.set_index('index'), rfe_imp, l1_imp, xgb_imp, rf_imp, ext_imp]:

        base = pd.merge(base, pd.melt(m.reset_index(), id_vars='index'), on=[
                        'index', 'variable'], how="left")

    col_names = ['f_score', 'mutual_info', 'rfe',
                 'l1_score', 'xgbs', 'rf', 'extra_tree']
    base.columns = ['feature', 'target'] + col_names

    for t in targets:

        print("Voting Summary for {}".format(t), "\n")

        sub = base[base['target'] == t].copy()
        sub = sub.drop(['target'], 1)
        normed = sub.iloc[:, 1:] / sub.iloc[:, 1:].sum()
        overall = normed.sum(axis=1)
        overall_normed = overall / overall.sum()
        sub['overall'] = overall_normed

        print(sub
              .sort_values(['overall'], ascending=False)
              .set_index('feature'))
        print("")

    return base
