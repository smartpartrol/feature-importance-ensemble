"""
Functions to create each measure of feature importance.
"""

import pandas as pd
import numpy as np
from sklearn.feature_selection import SelectFromModel, GenericUnivariateSelect, RFE

def get_univ_imp(df, func, features, y):
    """Generate Univariate feature importance for X and Y"""
    trans = GenericUnivariateSelect(
        score_func=func, mode='percentile', param=10)

    trans.fit(df[features], y=y)

    imps = pd.DataFrame(trans.scores_, index=features, columns=['importances'])

    return imps.sort_values('importances', ascending=True)


def make_univ_imp(df, uni_imp_funcs, features, targets):
    """Returns the F-Score and Mutul Info Score for Each Target relative to the features"""
    f_importances = []
    mu_importances = []

    for idx, name in enumerate(targets):

        print("Running F-Score Metric Selection for : {}".format(name))
        f_imp = get_univ_imp(df, uni_imp_funcs[0], features, df[name])
        f_importances.append((f_imp,))

        print("Running Mutual Info Selection for : {}".format(name))
        mu_imp = get_univ_imp(df, uni_imp_funcs[1], features, df[name])
        mu_importances.append((mu_imp,))

    print("Complete.", "\n")

    return f_importances, mu_importances


def stack_importances(imps, targets):
    """Stack into importance by target"""
    all_imps = imps[0][0].rename(columns={"importance": targets[0]})
    all_imps = all_imps.reset_index()

    for d in range(1, len(targets)):

        imp_sub = imps[d][0].rename(columns={"importance": targets[d]})
        imp_sub = imp_sub.reset_index()
        all_imps = pd.merge(all_imps, imp_sub, on="index", how="left")
        all_imps.columns = ['index'] + targets

    return all_imps


def make_trees(df, func, features, targets):
    """Run feature importance across provided targets and features
       Returns: a numpy matrix of feature importances"""
    imp_mat = np.empty((len(targets), len(features)))

    for t in range(len(targets)):

        func.fit(df[features], df[targets[t]])
        imp_mat[t, :] = func.feature_importances_

    return pd.DataFrame(imp_mat, columns=features, index=targets).T


def run_recursive_elmination(df, func, targets, features):
    """Returns DF of Recursively eliminated feature importance using Linear Regeression"""

    # create empty matrixes as place holders
    selected_features = np.empty((len(targets), len(features)))
    supports = np.empty((len(targets), len(features)))

    for idx, name in enumerate(targets):

        print("Running RFE for {}".format(name))
        rfe = RFE(func, 1, verbose=0)
        fitted = rfe.fit(df[features], df[name])
        support = rfe.get_support()

        selected_features[idx, :] = fitted.ranking_
        supports[idx, :] = rfe.support_
        print("Keeping only {} features :".format(np.sum(support)))
        print(np.array(features)[support])
        print("")

    recursive_features = pd.DataFrame(
        selected_features.T, columns=targets, index=features)
    recursive_features = recursive_features.rank(
        axis=0,  ascending=False, method="dense")

    return recursive_features


def get_l1_importance(df, func, targets, features):
    """Returns features selected via Lasso or L1 Regerssion"""
    l1_features = np.empty((len(targets), len(features)))
    l1_kept = np.empty((len(targets), len(features)))

    for idx, name in enumerate(targets):

        print("Target = {}".format(name))
        func.fit(df[features], df[name])
        sel = SelectFromModel(func, prefit=True)

        support = sel.get_support()
        l1_features[idx, :] = func.coef_
        l1_kept[idx, :] = support
        print("Keeping only {} features :".format(np.sum(support)))
        print(np.array(features)[support])
        print("")

    l1 = pd.DataFrame(l1_features, columns=features, index=targets).T
    # get them normed so same scale and magnitude
    return np.abs(l1) / np.sum(l1.apply(np.abs))
